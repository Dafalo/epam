package app.config;

import app.service.DirectoryManager;
import app.service.SearcherService;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    @Bean
    @ConfigurationProperties("path-properties")
    public PathProperties pathConfig() {
        return new PathProperties();
    }

    @Bean
    public DirectoryManager directoryManager() {
        return new DirectoryManager(pathConfig());
    }

    @Bean
    public SearcherService searcherService() {
        return new SearcherService(pathConfig());
    }
}
