package app.controllers;

import app.config.PathProperties;
import app.domain.CreateDto;
import app.domain.DeleteDto;
import app.service.DirectoryManager;
import app.service.NoteService;
import app.service.SearcherService;
import javafx.util.Pair;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@CrossOrigin
public class CatalogController {

    private final SearcherService searcherService;
    private final DirectoryManager directoryManager;
    private final NoteService noteService;
    private final String BASE_PATH;

    public CatalogController(SearcherService searcherService, DirectoryManager directoryManager, NoteService noteService,
                             PathProperties pathProperties) {

        this.searcherService = searcherService;
        this.directoryManager = directoryManager;
        this.noteService = noteService;
        BASE_PATH = pathProperties.getBasePath();
    }

    @GetMapping("/")
    public String getIndex(Model model) {
        List<File> directories = searcherService.getListOfDirectories(BASE_PATH);

        List<String> directoriesNames = directories.stream()
                .map(File::getName)
                .collect(Collectors.toList());

        model.addAttribute("directoriesNames", directoriesNames);

        List<String> filenames = searcherService.getListOfFiles(BASE_PATH);
        model.addAttribute("filenames", filenames);

        return "index";
    }

    @GetMapping("/directory")
    public String getDirectoryContent(@RequestParam String path, Model model) {
        System.out.println(BASE_PATH);

        List<File> directories = searcherService.getListOfDirectories(BASE_PATH + path);

        List<String> directoriesNames = directories.stream()
                .map(File::getName)
                .collect(Collectors.toList());
//        List<String> directoriesNames = searcherService.getListOfDirectories2(Paths.get(path));

        if (path.equals("/")) {
            model.addAttribute("parentPath", "");
        } else {
            model.addAttribute("currentPath", path);
            File file = new File(path);
            String parent = file.getParent().replaceAll("\\\\", "/");
            model.addAttribute("parentPath", parent);

        }

        List<Pair<String, String>> directoryToNote = new ArrayList<>();
        for (String dirName : directoriesNames) {
            String fullPath;
            if (path.equals("/")) {
                fullPath = BASE_PATH + File.separator + dirName;
            } else {
                fullPath = BASE_PATH + path.replaceAll("/", "\\\\") + File.separator + dirName;
            }
            String note = noteService.getLatest(fullPath);
            directoryToNote.add(new Pair<>(dirName, note));
        }

        model.addAttribute("directoriesNames", directoryToNote);

        List<String> filenames = searcherService.getListOfFiles(BASE_PATH + path);
        List<app.model.File> files = new ArrayList<>();
        for (String filename : filenames) {
            String fullPath;
            if (path.equals("/")) {
                fullPath = BASE_PATH + File.separator + filename;
            } else {
                fullPath = BASE_PATH + path.replaceAll("/", "\\\\") + File.separator + filename;
            }
            String note = noteService.getLatest(fullPath);
            app.model.File file = new app.model.File();
            file.setFileName(filename);
            file.setNote(note);
            file.setEdit(filename.endsWith(".txt"));
            files.add(file);
        }
        model.addAttribute("files", files);

        return "index";
    }


    @PutMapping("/create")
    @ResponseBody
    public void create(@RequestBody CreateDto body) {
        String path = BASE_PATH + body.getPath().replaceAll("/", "\\\\");
        String name = body.getName();
        if (body.isFile()) {
            directoryManager.createFile(path, name);
        } else {
            directoryManager.createDirectory(path, name);
        }
    }


    @DeleteMapping("/delete")
    @ResponseBody
    public void delete(@RequestBody DeleteDto body) {
        String path = BASE_PATH + "\\" + body.getPath().replaceAll("/", "\\\\");
        if (body.isFile()) {
            directoryManager.deleteFile(path);
        } else {
            directoryManager.deleteDirectory(path);
        }
    }
}
