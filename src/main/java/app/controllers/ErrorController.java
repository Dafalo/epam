package app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ErrorController {


    @GetMapping("/error-page")
    public String getErrorPage(Model model){
        return "error";
    }

}
