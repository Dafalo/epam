package app.controllers;

import app.config.PathProperties;
import app.service.FileService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class FileController {

    private final FileService fileService;
    private final String BASE_PATH;

    public FileController(FileService fileService, PathProperties pathProperties) {

        this.fileService = fileService;
        BASE_PATH = pathProperties.getBasePath();
    }

    @GetMapping("/edit")
    public String getFile(@RequestParam String path, Model model) {
        String fullPath = BASE_PATH + path.replaceAll("/", "\\\\");
        String file = fileService.getContent(fullPath);
        model.addAttribute("currentPath", path);
        model.addAttribute("file", file);

        return "file";
    }


    @PutMapping("/files")
    @ResponseBody
    public void createFile(@RequestBody CreateFileDto body) {
        String fullPath = BASE_PATH + body.getPath().replaceAll("/", "\\\\");
        String file = body.getFile();
        fileService.save(fullPath, file);
    }

    private static class CreateFileDto {
        private String path;
        private String file;

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getFile() {
            return file;
        }

        public void setFile(String file) {
            this.file = file;
        }
    }
}
