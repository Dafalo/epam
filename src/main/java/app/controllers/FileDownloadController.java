package app.controllers;

import app.config.PathProperties;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
@CrossOrigin
public class FileDownloadController {
    private final String BASE_PATH;

    public FileDownloadController(PathProperties pathProperties) {
        BASE_PATH = pathProperties.getBasePath();
    }

    @RequestMapping("/download")
    public void downloadFile(HttpServletResponse response, @RequestParam("fileName") String path) {

        String[] split = path.split("/");
        String fileName = split[split.length - 1];
        Path file = Paths.get(BASE_PATH + path);
        if (Files.exists(file)) {
            String mimeType = URLConnection.guessContentTypeFromName(path);
            if (mimeType == null) {
                mimeType = "application/octet-stream";
            }
            response.setContentType(mimeType);
            response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
            try {
                Files.copy(file, response.getOutputStream());
                response.getOutputStream().flush();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
