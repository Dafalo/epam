package app.controllers;

import app.config.PathProperties;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
@CrossOrigin
public class FileUploadController {
    private final String BASE_PATH;

    public FileUploadController(PathProperties pathProperties) {
        BASE_PATH = pathProperties.getBasePath();
    }

    @PostMapping("/upload")
    public String handleFileUpload(@RequestParam("file") MultipartFile file,
                                   @RequestParam String folder,
                                   RedirectAttributes redirectAttributes) {
        try {
            byte[] bytes = file.getBytes();

            String filename = file.getOriginalFilename();
            Path path = Paths.get(BASE_PATH, folder, filename);
            Files.write(path, bytes);
        } catch (IOException e) {
            redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
            return "redirect:/error-page";
        }

        String path = folder.equals("") ? "/" : folder;

        redirectAttributes.addAttribute("path", path);
        return "redirect:/directory";
    }
}
