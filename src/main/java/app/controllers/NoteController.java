package app.controllers;

import app.config.PathProperties;
import app.model.Note;
import app.service.NoteService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@CrossOrigin
public class NoteController {
    private final NoteService noteService;
    private final String BASE_PATH;


    public NoteController(NoteService noteService, PathProperties pathProperties) {
        this.noteService = noteService;
        BASE_PATH = pathProperties.getBasePath();
    }



    @GetMapping("/notes")
    public String getNote(@RequestParam String path, Model model) {
        String fullPath = BASE_PATH + path.replaceAll("/", "\\\\");

        String note = noteService.getLatest(fullPath);
        model.addAttribute("currentPath", path);
        model.addAttribute("note", note);

        List<Note> allNotes = noteService.getAll(fullPath);

        model.addAttribute("history", allNotes);

        return "note";
    }

    @GetMapping("/notes-create")
    public String getCreateNotePage(@RequestParam String path, Model model) {
        model.addAttribute("currentPath", path);

        return "create-note";
    }


    @PutMapping("/notes")
    @ResponseBody
    public void createNote(@RequestBody CreateNoteDto body){
        String fullPath = BASE_PATH + body.getPath().replaceAll("/", "\\\\");
        String note = body.getNote();
        noteService.save(fullPath, note);
    }

    private static class CreateNoteDto{
        private String path;
        private String note;

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }
    }
}
