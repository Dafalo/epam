package app.exceptions;

import java.io.IOException;

public class DirectoryAlreadyExists extends IOException {
    private static final String PATTERN = "The directory already exists.";


    public DirectoryAlreadyExists(String s) {
        super(String.format(PATTERN, s));
    }
}
