package app.service;

import app.config.PathProperties;
import app.exceptions.DirectoryAlreadyExists;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
public class DirectoryManager {
    private final String basePath;

    public DirectoryManager(PathProperties pathProperties) {
        this.basePath = pathProperties.getBasePath();
    }

    public void createDirectory(String path, String name) {
        Path path2 = Paths.get(path + "\\" + name);
        try {
            if (!Files.exists(path2)) {
                Files.createDirectory(path2);
            } else throw new DirectoryAlreadyExists(name);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void deleteDirectory(String path) {
        try {
            FileUtils.deleteDirectory(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createFile(String path, String name) {
        Path pathS = Paths.get(path, name);
        try {
            Files.createFile(pathS);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createFile2(Path path, String name) {
        Path pathS = Paths.get(basePath + path.toString(), name);
        try {
            Files.createFile(pathS);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createFile(String path, String name, List<String> content) {
        Path pathS = Paths.get(path, name);
        try {
            if (Files.exists(pathS)) {
                Files.write(pathS, content, StandardCharsets.UTF_8);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createFileWithContent(String path, byte[] content) {
        Path pathS = Paths.get(path);
        try {
            if (Files.exists(pathS)) {
                Files.write(pathS, content);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createFile(String path, String name, byte[] content) {
        Path pathS = Paths.get(path, name);
        try {
            if (Files.exists(pathS)) {
                Files.write(Paths.get(path), content);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteFile(String path) {
        try {
            Files.deleteIfExists(Paths.get(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteFile2(Path path) {
        try {
            Files.deleteIfExists(Paths.get(basePath + path.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
