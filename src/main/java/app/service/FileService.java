package app.service;

import app.model.File;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.stream.Collectors;

@Service
public class FileService {
    private final DirectoryManager directoryManager;

    public FileService(DirectoryManager directoryManager) {
        this.directoryManager = directoryManager;
    }

    public String getContent(String path) {
        String content = null;
        try {
            content = Files.lines(Paths.get(path)).collect(Collectors.joining());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    public void save(String fullPath, String content) {
        File file = new File();
        file.setContent(content);
        file.setInstant(Instant.now());

        directoryManager.createFileWithContent(fullPath, content.getBytes());
    }
}
