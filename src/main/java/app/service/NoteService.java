package app.service;

import app.config.PathProperties;
import app.model.Note;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class NoteService {

    private final Map<String, List<Note>> fullPathToNotes = new HashMap<>();



    public String getLatest(String path) {

        return fullPathToNotes.getOrDefault(path, Collections.emptyList())
                .stream()
                .max(Comparator.comparing(Note::getInstant))
                .map(Note::getContent)
                .orElse(null);
    }

    public List<Note> getAll(String path) {
        return fullPathToNotes.getOrDefault(path, new ArrayList<>())
                .stream()
                .sorted(Comparator.comparing(Note::getInstant).reversed())
                .collect(Collectors.toList());
    }

    public void save(String path, String content) {
        List<Note> notes = fullPathToNotes.getOrDefault(path, new ArrayList<>());

        Note note = new Note();
        note.setContent(content);
        note.setInstant(Instant.now());

        notes.add(note);

        fullPathToNotes.put(path, notes);
    }
}
