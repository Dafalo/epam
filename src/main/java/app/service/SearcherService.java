package app.service;

import app.config.PathProperties;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileFilter;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class SearcherService {
    private final String basePath;

    public SearcherService(PathProperties pathProperties) {
        this.basePath = pathProperties.getBasePath();
    }

    public List<File> getListOfDirectories(String path) {
        File mainDir = new File(path);
        FileFilter directoryFileFilter = file -> file.isDirectory() && file.exists();
        File[] directories = mainDir.listFiles(directoryFileFilter);
        return Arrays.asList(Objects.requireNonNull(directories));
    }

    public List<String> getListOfDirectories2(Path path) {
        File mainDir = new File(basePath + path.toString());
        FileFilter directoryFileFilter = file -> file.isDirectory() && file.exists();
        File[] directories = mainDir.listFiles(directoryFileFilter);
        List<File> directoriesList = Arrays.asList(Objects.requireNonNull(directories));
        return directoriesList.stream()
                .map(File::getName)
                .collect(Collectors.toList());
    }

    public List<String> getListOfFiles(String path) {
        File mainDir = new File(path);
        FileFilter directoryFileFilter = file -> file.isFile() && file.exists();
        File[] files = mainDir.listFiles(directoryFileFilter);

        if (files == null) {
            return Collections.emptyList();
        }

        return Arrays.stream(files).map(File::getName)
                .collect(Collectors.toList());

    }

    public List<String> getListOfFiles2(String path) {
        File mainDir = new File(basePath + path);
        FileFilter directoryFileFilter = file -> file.isFile() && file.exists();
        File[] files = mainDir.listFiles(directoryFileFilter);

        if (files == null) {
            return Collections.emptyList();
        }

        return Arrays.stream(files).map(File::getName)
                .collect(Collectors.toList());

    }
}
