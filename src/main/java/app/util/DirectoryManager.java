package app.util;

import app.exceptions.DirectoryAlreadyExists;
import org.apache.tomcat.util.http.fileupload.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class DirectoryManager {

    public void createDirectory(String path, String name) {
        Path path2 = Paths.get(path + "\\" + name);
        try {
            if (!Files.exists(path2)) {
                Files.createDirectory(path2);
            } else throw new DirectoryAlreadyExists(name);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void deleteDirectory(String path) throws IOException {
        FileUtils.deleteDirectory(new File(path));
    }

    public void createFile(String path, String name, List<String> content) {
        File file = new File(path + "\\" + name + ".txt");
        Path pathS = Paths.get(path);
        try {
            if (file.getParentFile().mkdirs() && file.createNewFile()) {
                Files.write(pathS, content, StandardCharsets.UTF_8);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createFile(String path, String name, byte[] content) {
        File file = new File(path + "\\" + name + ".txt");
        try {
            if (file.getParentFile().mkdirs() && file.createNewFile()) {
                Files.write(Paths.get(path), content);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteFile(String path) throws IOException {
        Files.deleteIfExists(Paths.get(path));
    }

    public static void main(String[] args) {
        String[] str = new String[]{"dffd"};
        new DirectoryManager().createFile("D:\\универ\\семестр2", "name", Arrays.asList(str));
        new DirectoryManager().createDirectory("D:\\универ\\семестр2", "name23");
    }
}
