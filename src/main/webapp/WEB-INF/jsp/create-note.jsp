<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <h3>Cоздание заметки для ${currentPath}</h3>
    <label for="create-note">Введите заметку: </label>
    <br>
    <textarea id="create-note" rows="20" cols="60"></textarea>
    <br>
    <button value="${currentPath}" onclick="create(this.value)">Создать</button>
    <button value="${currentPath}" onclick="back(this.value)">Назад</button>

</body>

<script language="JavaScript">

    function back(path) {
        window.location = "http://localhost:8080/directory?path=/" + path.split("/").slice(1, -1).join('/');
    }

    function create(path) {
        let body = {
            path: path,
            note: document.getElementById("create-note").value
        };
        const Http = new XMLHttpRequest();
        const url='http://localhost:8080/notes';
        Http.open("PUT", url, true);

        Http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");


        Http.onreadystatechange = (e) => {
            window.location = "http://localhost:8080/directory?path=/" + path.split("/").slice(1, -1).join('/');
        }
        Http.send(JSON.stringify(body));
    }
</script>

</html>
