<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>$Title$</title>
</head>
<body>
Файл ${currentPath}:
<p id="file">${file}</p>
<br>
<textarea hidden id="edit-file" rows="20" cols="60"></textarea>
<br>
<button id="edit-btn" value="${file}" onclick="edit(this.value)">Редактировать</button>
<button hidden id="save-btn" value="${currentPath}" onclick="save(this.value)">Сохранить</button>
<button hidden id="cancel-btn" onclick="cancel()">Отменить</button>
<button value="${currentPath}" onclick="back(this.value)">Назад</button>
<hr>
</body>
</html>
<script language="JavaScript">

    function edit(file) {
        document.getElementById("edit-file").hidden = false;
        document.getElementById("edit-file").value = file;
        document.getElementById("save-btn").hidden = false;
        document.getElementById("cancel-btn").hidden = false;

        document.getElementById("edit-btn").hidden = true;
        document.getElementById("file").hidden = true;
    }

    function cancel() {
        document.getElementById("edit-file").hidden = true;
        document.getElementById("save-btn").hidden = true;
        document.getElementById("cancel-btn").hidden = true;

        document.getElementById("edit-btn").hidden = false;
        document.getElementById("file").hidden = false;
    }

    function back(path) {
        window.location = "http://localhost:8080/directory?path=/" + path.split("/").slice(1, -1).join('/');
    }

    function save(path) {
        let body = {
            path: path,
            file: document.getElementById("edit-file").value
        };
        const Http = new XMLHttpRequest();
        const url='http://localhost:8080/files';
        Http.open("PUT", url, true);

        Http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");


        Http.onreadystatechange = (e) => {
            window.location = "http://localhost:8080/directory?path=/" + path.split("/").slice(1, -1).join('/');
        }
        Http.send(JSON.stringify(body));
    }
</script>

