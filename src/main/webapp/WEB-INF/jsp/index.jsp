<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>$Title$</title>
    <style>
        li {
            list-style-type: circle;
        }
    </style>
</head>
<body alink="#7FFFD4">

<p>Папки</p>
<ul>
    <c:forEach items="${directoriesNames}" var="directoryName">
        <li>
            <a href="/directory?path=${currentPath}/${directoryName.getKey()}">${directoryName.getKey()}</a>

            <c:if test="${directoryName.getValue() != null}">
                <br>
                <a href="/notes?path=${currentPath}/${directoryName.getKey()}">Прочитать заметку</a>
            </c:if>

            <c:if test="${directoryName.getValue() == null}">
                <br>
                <a href="/notes-create?path=${currentPath}/${directoryName.getKey()}">Создать заметку</a>
            </c:if>
        </li>
        <button type="submit" value="${currentPath}/${directoryName.getKey()}" onclick="remove(this.value, false)">
            Удалить
        </button>
    </c:forEach>

    <c:if test="${parentPath != ''}">
        <li><a href="/directory?path=${parentPath}">..</a></li>
    </c:if>
</ul>

<p>Создание новой папки</p>
<label for="create-dir">Введите имя: </label> <input type="text" id="create-dir"/>
<button value="${currentPath}" onclick="create(this.value, false)">Создать</button>

<hr><hr/>
<h2>Файлы</h2>
<form method="POST" enctype="multipart/form-data" action="/upload">
    <p>Загрузить файл: </p>
    <input type="file" name="file"><br/>
    <input type="submit" value="Загрузить">
    <input type="text" name="folder" value="${currentPath}" hidden>
</form>
<ul>
    <c:forEach items="${files}" var="file">
        <li>${file.fileName}</li>
        <c:if test="${file.note != null}">
            <br>
            <a href="/notes?path=${currentPath}/${file.fileName}">Прочитать заметку</a>
        </c:if>

        <c:if test="${file.note == null}">
            <br>
            <a href="/notes-create?path=${currentPath}/${file.fileName}">Создать заметку</a>
        </c:if>
        <c:if test="${file.edit}">
            <br>
            <a href="/edit?path=${currentPath}/${file.fileName}">Посмотреть файл</a>
        </c:if>

        <button type="submit" value="${currentPath}/${file.fileName}" onclick="remove(this.value, true)">Удалить
        </button>
        <p><a href="/download?fileName=${currentPath}/${file.fileName}" download>Скачать</a></p>
            <br><br/>
        <hr>
    </c:forEach>
</ul>
<p>Создание нового файла</p>
<label for="create-file">Введите имя: </label> <input type="text" id="create-file"/>
<button value="${currentPath}" onclick="create(this.value, true)">Создать</button>

</body>
<script language="JavaScript">

    function create(path, isFile) {
        let body = {
            file: isFile,
            path: path
        };
        if (isFile) {
            body.name = document.getElementById("create-file").value;
        } else {
            body.name = document.getElementById("create-dir").value;
        }
        const Http = new XMLHttpRequest();
        const url = 'http://localhost:8080/create';
        Http.open("PUT", url, true);

        Http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        Http.onreadystatechange = (e) =>
        {
            location.reload(true);
        }
        Http.send(JSON.stringify(body));
    }

    function remove(path, isFile) {
        if (path.startsWith("/")) {
            path = path.slice(1);
        }
        const Http = new XMLHttpRequest();
        const url = 'http://localhost:8080/delete';
        Http.open("DELETE", url, true);

        Http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        const body = {
            path: path,
            file: isFile
        };

        Http.onreadystatechange = (e) =>
        {
            location.reload(true);
        }
        Http.send(JSON.stringify(body));
    }
</script>
</html>