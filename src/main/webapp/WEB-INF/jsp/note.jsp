<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>$Title$</title>
</head>
<body>
    Заметка для ${currentPath}:
    <p id="note">${note}</p>
    <br>
    <textarea hidden id="edit-note" rows="20" cols="60"></textarea>
    <br>
    <button id="edit-btn" value="${note}" onclick="edit(this.value)">Редактировать</button>
    <button hidden id="save-btn" value="${currentPath}" onclick="save(this.value)">Сохранить</button>
    <button hidden id="cancel-btn" onclick="cancel()">Отменить</button>
    <button value="${currentPath}" onclick="back(this.value)">Назад</button>

    <hr>

    <c:forEach items="${history}" var="entry">
        <p>От ${entry.getInstant()}:</p>
        <p>${entry.getContent()}</p>
        <br>
    </c:forEach>

</body>
</html>
<script language="JavaScript">

    function edit(note) {
        document.getElementById("edit-note").hidden = false;
        document.getElementById("edit-note").value = note;
        document.getElementById("save-btn").hidden = false;
        document.getElementById("cancel-btn").hidden = false;

        document.getElementById("edit-btn").hidden = true;
        document.getElementById("note").hidden = true;
    }

    function cancel() {
        document.getElementById("edit-note").hidden = true;
        document.getElementById("save-btn").hidden = true;
        document.getElementById("cancel-btn").hidden = true;

        document.getElementById("edit-btn").hidden = false;
        document.getElementById("note").hidden = false;
    }

    function back(path) {
        window.location = "http://localhost:8080/directory?path=/" + path.split("/").slice(1, -1).join('/');
    }

    function save(path) {
        let body = {
            path: path,
            note: document.getElementById("edit-note").value
        };
        const Http = new XMLHttpRequest();
        const url='http://localhost:8080/notes';
        Http.open("PUT", url, true);

        Http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");


        Http.onreadystatechange = (e) => {
            window.location = "http://localhost:8080/directory?path=/" + path.split("/").slice(1, -1).join('/');
        }
        Http.send(JSON.stringify(body));
    }
</script>
